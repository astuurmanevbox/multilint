#!/bin/sh
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -eu

DOCKER_ARGS="--rm -v \"$(pwd):/workdir\" -w \"/workdir\""
CHECKMAKE_ARGS=""
CHECKMAKE_REPO="registry.hub.docker.com/mandrean/checkmake:latest"
HADOLINT_ARGS="-f tty"
HADOLINT_REPO="registry.hub.docker.com/hadolint/hadolint:latest-debian"
LUACHECK_ARGS="-q"
LUACHECK_REPO="registry.hub.docker.com/thibf/luacheck:latest"
SHELLCHECK_ARGS="-x -C -f tty -s sh"
SHELLCHECK_REPO="registry.hub.docker.com/koalaman/shellcheck:stable"
YAMLLINT_REPO="registry.hub.docker.com/pipelinecomponents/yamllint:latest"
YAMLLINT_ARGS="-d '{extends: default, rules: {document-start: {present: false}, line-length: {max: 120}}}'"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "Run this repositories linters"
    echo "    -d   Run hadolint on Dockerfiles"
    echo "    -h   Print usage"
    echo "    -l   Run luacheck on lua scripts"
    echo "    -m   Run checkmake on Makefiles"
    echo "    -n   Do not use docker for checks"
    echo "    -o   Always run offline"
    echo "    -s   Run shellcheck on shell scripts"
    echo "    -y   Run YAML lint"
    echo
    echo "Without any option, all linters are run."
}

run_yamllint()
{
    if [ -n "${docker}" ] 1> "/dev/null"; then
        if [ -z "${offline:-}" ]; then
            docker pull "${YAMLLINT_REPO}" 1> /dev/null || true
        fi
        YAMLLINT_CMD="docker run ${DOCKER_ARGS} ${YAMLLINT_REPO} yamllint"
    elif command -v "yamllint" 1> /dev/null; then
        YAMLLINT_CMD="yamllint"
    else
        echo "Neither 'yamllint' nor 'docker' are installed to run the yaml linter."
        return
    fi

    if [ -f "${1:-}" ]; then
        eval "${YAMLLINT_CMD}" "${YAMLLINT_ARGS}" "${*}"
    else
        find '.' \
            -type f \
            -iname '*.yaml' -o -iname '.*.yaml' -o -iname '*.yml' -o -iname '.*.yml' | while read -r file; do
                echo "Running yamllint on '${file}'."
                if eval "${YAMLLINT_CMD}" "${YAMLLINT_ARGS}" "${file}"; then
                    echo "  No problems detected."
                fi
        done
    fi
}

run_shellcheck()
{
    if [ -n "${docker}" ] 1> "/dev/null"; then
        if [ -z "${offline:-}" ]; then
            docker pull "${SHELLCHECK_REPO}" 1> /dev/null || true
        fi
        SHELLCHECK_CMD="docker run ${DOCKER_ARGS} ${SHELLCHECK_REPO}"
    elif command -v "shellcheck" 1> /dev/null; then
        SHELLCHECK_CMD="shellcheck"
    else
        echo "Neither 'shellcheck' nor 'docker' are installed to run the shellcheck linter."
        return
    fi

    if [ -f "${1:-}" ]; then
        eval "${SHELLCHECK_CMD}" "${SHELLCHECK_ARGS}" "${*}"
    else
        find '.' \
            -type f \
            -name '*.sh' | while read -r file; do
                echo "Running shellcheck on '${file}'."
                if eval "${SHELLCHECK_CMD}" "${SHELLCHECK_ARGS}" "${file}"; then
                    echo "  No problems detected."
                fi
        done
    fi
}

run_luacheck()
{
    if [ -n "${docker}" ] 1> "/dev/null"; then
        if [ -z "${offline:-}" ]; then
            docker pull "${LUACHECK_REPO}" 1> /dev/null || true
        fi
        LUACHECK_CMD="docker run ${DOCKER_ARGS} ${LUACHECK_REPO} luacheck"
    elif command -v "luacheck" 1> /dev/null; then
        LUACHECK_CMD="luacheck"
    else
        echo "Neither 'luacheck' nor 'docker' are installed to run the luacheck linter."
        return
    fi

    if [ -f "${1:-}" ]; then
        eval "${LUACHECK_CMD}" "${LUACHECK_ARGS}" "${*}"
    else
        find '.' \
            -type f \
            -name '*.lua' | while read -r file; do
                if eval "${LUACHECK_CMD}" "${LUACHECK_ARGS}" "${file}"; then
                    echo "  No problems detected in '${file}'"
                fi
        done
    fi
}

run_hadolint()
{
    if [ -n "${docker}" ] 1> "/dev/null"; then
        if [ -z "${offline:-}" ]; then
            docker pull "${HADOLINT_REPO}" 1> /dev/null || true
        fi
        HADOLINT_CMD="docker run ${DOCKER_ARGS} ${HADOLINT_REPO} hadolint"
    elif command -v "hadolint" 1> /dev/null; then
        HADOLINT_CMD="hadolint"
    else
        echo "Neither 'hadolint' nor 'docker' are installed to run the hadolint linter."
        return
    fi

    if [ -f "${1:-}" ]; then
        eval "${HADOLINT_CMD}" "${HADOLINT_ARGS}" "${*}"
    else
        find '.' \
            -type f \
            -name 'Dockerfile' | while read -r file; do
                echo "Running hado lint on '${file}'."
                if eval "${HADOLINT_CMD}" "${HADOLINT_ARGS}" "${file}"; then
                    echo "  No problems detected."
                fi
        done
    fi
}

run_checkmake()
{
    if [ -n "${docker}" ] 1> "/dev/null"; then
        if [ -z "${offline:-}" ]; then
            docker pull "${CHECKMAKE_REPO}" 1> /dev/null || true
        fi
        CHECKMAKE_CMD="docker run ${DOCKER_ARGS} ${CHECKMAKE_REPO}"
    elif command -v "checkmake" 1> /dev/null; then
        CHECKMAKE_CMD="checkmake"
    else
        echo "Neither 'checkmake' nor 'docker' are installed to run the Makefile linter."
        return
    fi

    if [ -f "${1:-}" ]; then
        eval "${CHECKMAKE_CMD}" "${CHECKMAKE_ARGS}" "${*}"
    else
        find '.' \
            -type f \
            -name 'Makefile' | while read -r file; do
                echo "Running checkmake on '${file}'."
                if eval "${CHECKMAKE_CMD}" "${CHECKMAKE_ARGS}" "${file}"; then
                    echo "  No problems detected."
                fi
        done
    fi
}

main()
{
    docker="$(command -v "docker")"

    while getopts ":dhlmnosy" options; do
        case "${options}" in
        d)
            run_linters="${run_linters:-} run_hadolint"
            ;;
        h)
            usage
            exit 0
            ;;
        l)
            run_linters="${run_linters:-} run_luacheck"
            ;;
        m)
            run_linters="${run_linters:-} run_checkmake"
            ;;
        n)
            docker=""
            ;;
        o)
            offline="true"
            ;;
        s)
            run_linters="${run_linters:-} run_shellcheck"
            ;;
        y)
            run_linters="${run_linters:-} run_yamllint"
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    case "${0##*/}" in
    checkmake.sh)
        run_linters="run_checkmake"
        ;;
    hadolint.sh)
        run_linters="run_hadolint"
        ;;
    luacheck.sh)
        run_linters="run_luacheck"
        ;;
    shellcheck.sh)
        run_linters="run_shellcheck"
        ;;
    yamllint.sh)
        run_linters="run_yamllint"
        ;;
    *)
        if [ -z "${run_linters:-}" ]; then
            run_linters="
                run_hadolint
                run_shellcheck
                run_luacheck
                run_checkmake
                run_yamllint
            "
        fi
    esac

    for linter in ${run_linters}; do
        ${linter} "${1:-}"
    done
}

main "${@}"

exit 0
